const initialState = {
  selectedCommand: 0,
  commands: []
};

export default function(state = initialState, action) {
  console.log('reducer', action);
  switch (action.type) {
    case 'addCommand':
      return Object.assign({}, state, {commands: state.commands.concat(action.command)})
    case 'selectCommand':
      return Object.assign({}, state, {selectedCommand: action.i})
    default:
      return state;
  }
}
