import React, { Component } from 'react';
import './CommandListViewer.css';
import { connect } from 'react-redux';

class CommandListViewer extends Component {
  selectItem(i) {
    this.props.dispatch({type: 'selectCommand', i: i})
  }
  render() {
    return (
      <div className="command-log" >
      {this.props.commands.map((command, i) =>
        <div className={'command-log-item' + (i === this.props.selectedCommand ? ' selected' : '')} key={i}
          onClick={()=>this.selectItem(i)}
          >{command}</div>)}
      </div>
    )
  }
}

const mapStateToProps = function(state) {
  return {
    commands: state.commands,
    selectedCommand: state.selectedCommand
  }
}

export default connect(mapStateToProps)(CommandListViewer);
