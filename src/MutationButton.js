import React, { Component } from 'react';
import { connect } from 'react-redux';
import './MutationButton.css';

class ParamForm extends Component {
  disabled() {
    return false;
  }
  click() {
    this.props.dispatch({type: 'addCommand', command: this.props.mutation})
  }
  render(props) {
    return (
      <button className="mutation-button" style={{background:'url('+ this.props.image + ') center / contain no-repeat'}} disabled={this.disabled()}
      onClick={()=>this.click()}
      title={this.props.title || this.props.mutation}>
    <span>{this.props.text}</span>
  </button>
    )
  }
}

export default connect()(ParamForm);
