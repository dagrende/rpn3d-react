import React, { Component } from 'react';
import { connect } from 'react-redux';
import ModelViewer from './ModelViewer.js';
import ParamForm from './ParamForm.js';
import MutationButton from './MutationButton.js';
import CommandListViewer from './CommandListViewer.js';

import './App.css';

import unknown_user from './assets/unknown-user.png'
import cubeIcon from './assets/cube-icon.png'
import cylinderIcon from './assets/cylinder-icon.png'
import torusIcon from './assets/torus-icon.png'
import sphereIcon from './assets/sphere-icon.png'
import unionIcon from './assets/union-icon.svg'
import differenceIcon from './assets/difference-icon.svg'
import intersectionIcon from './assets/intersection-icon.svg'
import translateIcon from './assets/translate-icon.svg'
import scaleIcon from './assets/scale-icon.svg'
import rotateIcon from './assets/rotate-icon.svg'
import fullscreenIcon from './assets/fullscreen-icon.svg'

class App extends Component {
  toggleFullscreen() {
    console.log('toggleFullscreen');
  }
  editTitle() {
    console.log('editTitle');
  }
  commitTitleEdit() {
    console.log('commitTitleEdit');
  }
  svgTextIcon(text) {
    return `data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22utf-8%22%3F%3E%0A%3C%21DOCTYPE%20svg%20PUBLIC%20%22-%2F%2FW3C%2F%2FDTD%20SVG%201.1%2F%2FEN%22%20%22http%3A%2F%2Fwww.w3.org%2FGraphics%2FSVG%2F1.1%2FDTD%2Fsvg11.dtd%22%3E%0A%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%0A%20%20version%3D%221.1%22%20baseProfile%3D%22full%22%20width%3D%22100%22%20height%3D%22100%22%20viewBox%3D%220%200%2024.00%2024.00%22%20enable-background%3D%22new%200%200%2024.00%2024.00%22%20xml%3Aspace%3D%22preserve%22%3E%0A%20%20%3Ctext%20x%3D%225%22%20y%3D%2216%22%20font-family%3D%22Verdana%22%20font-size%3D%227%22%20%20stroke%3D%22none%22%20fill%3D%22black%22%3E${text}%3C%2Ftext%3E%0A%3C%2Fsvg%3E%0A`
  }
  download() {
    console.log('download');
  }
  commit(s) {
    console.log('commit', s);
    this.props.dispatch(s)
  }
  open() {
    console.log('open');
  }
  save() {
    console.log('save');
  }
  render() {
    return (
      <div ref="fullscreen" id="app">
        <ModelViewer ref="viewerCanvas" stackIndex="0" className="model-viewer top"/>
        <div className="top-row">
          <button className="fs" type="button" onClick={() => this.toggleFullscreen()} style={{background:'url(' + fullscreenIcon + ') center / contain no-repeat'}}></button>
          <span className="title" onClick={() => this.editTitle()}>A title</span>
          <input className="title-edit" ref="titleField" hidden={true} onKeyUp={(e) => this.commitTitleEdit(e)} type="text"></input>
          <button className="user-image" type="button" onClick={() => this.loginOut()} style={{background:'url(' + unknown_user + ') center / contain no-repeat'}}></button>
        </div>
        <div className="main">
          <div className="left-fill">
            <div ref="viewer" className="viewer">
            </div>
            <div className="buttons">
                <ParamForm className="field-row"/>
                <div className="button-row">
                  <MutationButton image={cubeIcon} mutation="addCube" title="cube"/>
                  <MutationButton image={cylinderIcon} mutation="addCylinder" title="cylinder"/>
                  <MutationButton image={sphereIcon} mutation="addSphere" title="sphere"/>
                  <MutationButton image={torusIcon} mutation="addTorus" title="torus"/>
                  <MutationButton image={cubeIcon} mutation="importStl" title="stl object"/>
                  <MutationButton image={this.svgTextIcon('RCL')} mutation="addNamedObject" title="add named object"/>
                  <MutationButton image={this.svgTextIcon('POP')} mutation="popStack" title="remove top of stack"/>
                  <MutationButton image={this.svgTextIcon('DUP')} mutation="dupStack" title="duplicates top of stack"/>
                  <MutationButton image={this.svgTextIcon('SWAP')} mutation="swapStack" title="swap top two stack items"/>
                </div>
                <div className="button-row">
                  <MutationButton image={unionIcon} mutation="union"/>
                  <MutationButton image={differenceIcon} mutation="subtract"/>
                  <MutationButton image={intersectionIcon} mutation="intersect"/>
                  <MutationButton image={translateIcon} mutation="translate"/>
                  <MutationButton image={scaleIcon} mutation="scale"/>
                  <MutationButton image={rotateIcon} mutation="rotate"/>
                  <MutationButton image={this.svgTextIcon('ALGN')} mutation="align"/>
                  <MutationButton image={this.svgTextIcon('MIRR')} mutation="mirror"/>
                  <MutationButton image={cubeIcon} mutation="addEnclosingBlock" title="enclosing block"/>
                  <MutationButton image={cubeIcon} mutation="growBlock" title="grow block"/>
                  <MutationButton image={this.svgTextIcon('STO')} mutation="nameTop" title="name current object"/>
                  <MutationButton image={this.svgTextIcon('#')} mutation="comment" title="#"/>
                  <MutationButton image={this.svgTextIcon('RPT')} mutation="repeat" title="repeat"/>
                  <MutationButton image={this.svgTextIcon('CNST')} mutation="const" title="const"/>
                </div>
                <div className="button-row">
                  <button type="button" onClick={() => this.open()}>open</button>
                  <button type="button" onClick={() => this.save()}>save</button>
                  <button type="button" onClick={() => this.commit('deleteLogRow')}>delete</button>
                  <button type="button" onClick={() => this.commit('undo')}>undo</button>
                  <button type="button" onClick={() => this.commit('redo')}>redo</button>
                  <button type="button" onClick={() => this.download()}>download</button>
                </div>
            </div>
          </div>
          <CommandListViewer className="right command-list"/>
        </div>
      </div>
    );
  }
}

export default connect()(App);
